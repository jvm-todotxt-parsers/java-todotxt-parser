package org.egedede.todotxt;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestTodotxtListParser {

  private String defaultContent = """
      (A) Thank Mom for the meatballs @phone
      (B) Schedule Goodwill pickup +GarageSale @phone
      Post signs around the neighborhood +GarageSale
      @GroceryStore Eskimo pies
      x 2011-03-02 2011-03-01 Review Tim's pull request +TodoTxtTouch @github
      x 2011-03-03 Call Mom
      2011-03-02 Document +TodoTxt task format
      (A) 2011-03-02 Call Mom
          
          
        """;

  @Test
  @DisplayName("Parse a string")
  void testParseAString() {
    List<Task> result = new TodotxtListParser(new DefaultTaskParser()).parse(defaultContent);
    Assertions.assertAll(
        "Regular reading",
        () -> MatcherAssert.assertThat(result, Matchers.hasSize(8))
    );
  }
}
