package org.egedede.todotxt;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.format.DateTimeFormatter;

public class TestDefaultTaskParser {


  @Test
  @DisplayName("Parse a complete task")
  void testParseACompleteTask() {
    Task result = new DefaultTaskParser().parse("x 2011-03-02 2011-03-01 Review Tim's pri:A pull +plop request +TodoTxtTouch @github");
    Assertions.assertAll(
        "Regular reading",
        () -> MatcherAssert.assertThat(result.getCompletionDate().isPresent(), Matchers.is(true)),
        () -> MatcherAssert.assertThat(result.getCreationDate().isPresent(), Matchers.is(true)),
        () -> MatcherAssert.assertThat(result.getCompletionDate().get().format(DateTimeFormatter.ISO_LOCAL_DATE), Matchers.is("2011-03-02")),
        () -> MatcherAssert.assertThat(result.getCreationDate().get().format(DateTimeFormatter.ISO_LOCAL_DATE), Matchers.is("2011-03-01")),
        () -> MatcherAssert.assertThat(result.getProjects(), Matchers.contains("plop", "TodoTxtTouch")),
        () -> MatcherAssert.assertThat(result.getContexts(), Matchers.contains("github")),
        () -> MatcherAssert.assertThat(result.getTags(), Matchers.contains(new Tag("pri", "A"))),
        () -> MatcherAssert.assertThat(result.getContent(), Matchers.is("Review Tim's pri:A pull +plop request +TodoTxtTouch @github")),
        () -> MatcherAssert.assertThat(result.getRawData(), Matchers.is("x 2011-03-02 2011-03-01 Review Tim's pri:A pull +plop request +TodoTxtTouch @github"))

    );
  }

  @Test
  @DisplayName("Parse an incomplete task")
  void testParseAnIncompleteTask() {
    Task result = new DefaultTaskParser().parse("(A) Call Mom +Family +PeaceLoveAndHappiness @iphone @phone due:2021-03-21 t");
    Assertions.assertAll(
        "Regular reading",
        () -> MatcherAssert.assertThat(result.getCompletionDate().isPresent(), Matchers.is(false)),
        () -> MatcherAssert.assertThat(result.getCreationDate().isPresent(), Matchers.is(false)),
        () -> MatcherAssert.assertThat(result.getPriority(), Matchers.is('A')),
        () -> MatcherAssert.assertThat(result.getProjects(), Matchers.containsInAnyOrder("Family", "PeaceLoveAndHappiness")),
        () -> MatcherAssert.assertThat(result.getContexts(), Matchers.containsInAnyOrder("iphone", "phone")),
        () -> MatcherAssert.assertThat(result.getTags(), Matchers.contains(new Tag("due", "2021-03-21"))),
        () -> MatcherAssert.assertThat(result.getContent(), Matchers.is("Call Mom +Family +PeaceLoveAndHappiness @iphone @phone due:2021-03-21 t")),
        () -> MatcherAssert.assertThat(result.getRawData(), Matchers.is("(A) Call Mom +Family +PeaceLoveAndHappiness @iphone @phone due:2021-03-21 t"))

    );
  }
}
