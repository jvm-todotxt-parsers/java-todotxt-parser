package org.egedede.todotxt;

import java.util.Objects;

public class Tag {
  private final String key;
  private final String value;

  public Tag(String key, String value) {
    this.key = key;
    this.value = value;
  }

  public String getKey() {
    return key;
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Tag tag = (Tag) o;
    return Objects.equals(key, tag.key) && Objects.equals(value, tag.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(key, value);
  }

  @Override
  public String toString() {
    return "Tag{" +
        "key='" + key + '\'' +
        ", value='" + value + '\'' +
        '}';
  }
}
