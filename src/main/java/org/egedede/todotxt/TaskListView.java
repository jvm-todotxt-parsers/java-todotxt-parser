package org.egedede.todotxt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskListView {

  /** Should be able to be applied in any order. */
  private List<TaskFilter> filters = new ArrayList<>(10);

  /** Should be applied in order */
  private List<Comparator<Task>> sorters = new ArrayList<>(10);

  public List<Task> apply(Collection<Task> taskList) {
    final AtomicReference<Stream<Task>> stream = new AtomicReference<>(taskList.stream());
    // filter first there should be less to sort after.
    filters.forEach(
        f -> {
          stream.set(stream.get().filter(t -> f.matches(t)));
        });
    sorters.forEach(
        s -> {
          stream.set(stream.get().sorted(s));
        });
    return stream.get().collect(Collectors.toList());
  }

  public void add(TaskFilter filter) {
    filters.add(filter);
  }
  public void remove(TaskFilter filter) {
    filters.remove(filter);
  }
  public void add(Comparator<Task> comparator) {
    sorters.add(comparator);
  }
  public void remove(Comparator<Task> comparator) {
    sorters.remove(comparator);
  }


}
