package org.egedede.todotxt;

import java.util.*;

public class TaskList {

  private Collection<Task> tasks;
  private TaskListView view;

  public TaskList(Collection<Task> tasks) {
    this.tasks = new TreeSet<>(tasks);
  }

  public void add(Task task) {
    tasks.add(task);
  }
  public boolean remove(Task task) {
    return tasks.remove(task);
  }

  public void addFilter(TaskFilter filter) {
    view.add(filter);
  }

  public List<Task> getVisibleTasks() {
    return view.apply(tasks);
  }
}
