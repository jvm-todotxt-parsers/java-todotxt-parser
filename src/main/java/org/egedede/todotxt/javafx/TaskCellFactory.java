package org.egedede.todotxt.javafx;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;
import org.egedede.todotxt.Task;

public class TaskCellFactory implements Callback<ListView<Task>, ListCell<Task>> {
  @Override
  public ListCell<Task> call(ListView<Task> param) {
    return new ListCell<>() {
      @Override
      protected void updateItem(Task item, boolean empty) {
        super.updateItem(item, empty);
        if(empty) {
          setText( null);
          setGraphic(null);
        } else if (item == null){
          setText("No item :thinking_face:");
          setGraphic(null);
        } else {
          // do the real stuff here
          setText(null);
          setGraphic(new TaskView(item));
        }
      }
    };
  }
}
