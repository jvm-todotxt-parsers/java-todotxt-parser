package org.egedede.todotxt.javafx;

import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;
import org.egedede.todotxt.Task;

import java.time.format.DateTimeFormatter;

/**
 * View of a task * left due date or closed date * middle the content * right tags * color for the
 * priority.
 */
public class TaskView extends HBox {

  public TaskView(Task task) {
    super(10d);
    setPadding(new Insets(10d));
    setBackground(new Background(new BackgroundFill(getColor(task.getPriority()), new CornerRadii(10d), Insets.EMPTY)));
    getChildren()
        .add(
            new Text(
                task.getCompletionDate()
                    .or(task::getCreationDate)
                    .map(d -> d.format(DateTimeFormatter.ISO_DATE))
                    .orElse("No date")));
    getChildren().add(new Text(task.getContent()));
  }

  private Paint getColor(char priority) {
    return switch (priority) {
      case 'A' -> Color.RED;
      case 'B' -> Color.YELLOW;
      case 'C' -> Color.GREEN;
      case (char) -1 -> Color.WHITE;
      default -> Color.LIGHTGRAY;
    };
  }
}
