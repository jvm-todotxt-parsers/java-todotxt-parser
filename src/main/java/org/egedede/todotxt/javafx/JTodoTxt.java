package org.egedede.todotxt.javafx;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import org.egedede.todotxt.DefaultTaskParser;
import org.egedede.todotxt.Task;
import org.egedede.todotxt.TodotxtListParser;
import org.tinylog.Logger;

import java.util.List;

public class JTodoTxt extends Application {
  private final TodotxtListParser parser = new TodotxtListParser(new DefaultTaskParser());

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) {
    TextArea textArea = new TextArea("""
      (A) Thank Mom for the meatballs @phone
      (B) Schedule Goodwill pickup +GarageSale @phone
      Post signs around the neighborhood +GarageSale
      @GroceryStore Eskimo pies
      x 2011-03-02 2011-03-01 Review Tim's pull request +TodoTxtTouch @github
      x 2011-03-03 Call Mom
      2011-03-02 Document +TodoTxt task format
      (A) 2011-03-02 Call Mom
          
          
        """);
    //now create a view associated with this text Area

    ListView<Task> taskListView = new ListView<>(FXCollections.observableList(parser.parse(textArea.getText())));
    taskListView.setCellFactory(new TaskCellFactory());
    textArea.textProperty().addListener((observable, oldValue, newValue) -> {
      Logger.info("Change event :  build logic to update tasks view");
      List<Task> tasks = parser.parse(newValue);
      taskListView.getItems().clear();
      taskListView.getItems().addAll(tasks);
    });
    SplitPane main = new SplitPane(textArea, taskListView);
    Scene scene = new Scene(main, 600, 400);
    primaryStage.setScene(scene);
    primaryStage.show();




  }
}
