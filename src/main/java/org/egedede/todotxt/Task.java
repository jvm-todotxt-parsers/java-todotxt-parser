package org.egedede.todotxt;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class Task implements Cloneable {
  private String rawData;
  private boolean done;
  /**
   * 0 means no priority
   */
  private char priority = (char)-1;
  private Optional<LocalDate> completionDate = Optional.empty();
  private Optional<LocalDate> creationDate = Optional.empty();
  private String content;
  private List<String> projects;
  private List<String> contexts;
  private List<Tag> tags;

  public String getRawData() {
    return rawData;
  }

  public void setRawData(String rawData) {
    this.rawData = rawData;
  }

  public boolean isDone() {
    return done;
  }

  public void setDone(boolean done) {
    this.done = done;
  }

  public char getPriority() {
    return priority;
  }

  public void setPriority(char priority) {
    this.priority = priority;
  }

  public Optional<LocalDate> getCompletionDate() {
    return completionDate;
  }

  public void setCompletionDate(Optional<LocalDate> completionDate) {
    this.completionDate = completionDate;
  }

  public Optional<LocalDate> getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Optional<LocalDate> creationDate) {
    this.creationDate = creationDate;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public List<String> getProjects() {
    return projects;
  }

  public void setProjects(List<String> projects) {
    this.projects = projects;
  }

  public List<String> getContexts() {
    return contexts;
  }

  public void setContexts(List<String> contexts) {
    this.contexts = contexts;
  }

  public List<Tag> getTags() {
    return tags;
  }

  public void setTags(List<Tag> tags) {
    this.tags = tags;
  }

  @Override
  public Task clone() {
    Task clone = new Task();
    return clone;
  }
}
