package org.egedede.todotxt;

public interface TaskFilter {
  boolean matches(Task task);
}
