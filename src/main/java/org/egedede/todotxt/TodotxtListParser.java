package org.egedede.todotxt;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TodotxtListParser {

  private final TodotxtTaskParser taskParser;

  public TodotxtListParser(TodotxtTaskParser taskParser) {
    this.taskParser = taskParser;
  }

  public List<Task> parse(String todoContent) {
    return todoContent
        .lines()
        .map(taskParser::parse)
        .filter(Objects::nonNull)
        .collect(Collectors.toList());
  }

  public List<Task> parse(InputStream stream) throws IOException {
    return parse(new String(stream.readAllBytes(), StandardCharsets.UTF_8));
  }
}
