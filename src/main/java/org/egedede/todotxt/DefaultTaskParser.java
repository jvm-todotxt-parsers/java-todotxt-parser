package org.egedede.todotxt;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DefaultTaskParser implements TodotxtTaskParser {

  public static final String ISO_DATE_REGEXP = "[0-9]{4}-[0-9]{2}-[0-9]{2}";

  @Override
  public Task parse(String line) {
    Task task;
    if(line.trim().length()==0) {
      task = null;
    }else if (line.startsWith("x ")) {
      task = parseCompleteTask(line);
    } else {
      task = parseIncompleteTask(line);
    }
    return task;
  }

  private Task parseCompleteTask(String line) {
    Task task = new Task();
    task.setRawData(line);
    task.setDone(true);
    task.setCompletionDate(Optional.of(LocalDate.parse(line.substring(2, 12))));
    String content;
    if (line.length()>=23 && line.substring(13, 23).matches(ISO_DATE_REGEXP)) {
      task.setCreationDate(Optional.of(LocalDate.parse(line.substring(13, 23))));
      content = line.substring(24);
    } else {
      content = line.substring(13);
    }
    task.setContent(content);
    task.setProjects(getProjects(content));
    task.setContexts(getContexts(content));
    task.setTags(getTags(content));
    return task;
  }
  private Task parseIncompleteTask(String line) {
    String dateRegexp = ISO_DATE_REGEXP;
    Task task = new Task();
    task.setRawData(line.trim());
    task.setDone(false);
    int index = 0;
    if(line.length()>3 && line.substring(index, index+3).matches("\\([A-Z]\\)")){
      index=4;
      task.setPriority(line.charAt(1));
    } else {
      task.setPriority((char) 0);
    }
    if(line.length()>10 && line.substring(index, index+10).matches(dateRegexp)) {
      task.setCreationDate(Optional.of(LocalDate.parse(line.substring(index, index+10))));
    }
    String content = line.substring(index);
    task.setContent(content);
    List<String> projects = getProjects(content);
    task.setProjects(projects);
    task.setContexts(getContexts(content));
    task.setTags(getTags(content));
    return task;
  }

  private List<String> getProjects(String content) {
    Pattern projectPattern = Pattern.compile("\\+([^\s]+)");
    List<String> projects = new ArrayList<>();
    Matcher projectMatcher = projectPattern.matcher(content);
      while(projectMatcher.find()){
        projects.add(projectMatcher.group(1));
      }
    return projects;
  }
  private List<String> getContexts(String content) {
    Pattern projectPattern = Pattern.compile("@([^\s]+)");
    List<String> projects = new ArrayList<>();
    Matcher projectMatcher = projectPattern.matcher(content);
      while(projectMatcher.find()){
        projects.add(projectMatcher.group(1));
      }
    return projects;
  }

  private List<Tag> getTags(String content) {
    Pattern tagsPattern = Pattern.compile("\s?([a-zA-Z0-9_\\-]+:[a-zA-Z0-9_\\-]+)\s?");
    List<Tag> tags = new ArrayList<>();
    Matcher tagsMatcher = tagsPattern.matcher(content);
    while(tagsMatcher.find()){
      String[] tagParts = tagsMatcher.group(1).split(":");
      tags.add(new Tag(tagParts[0], tagParts[1]));
    }
    return tags;
  }
}
