package org.egedede.todotxt;

public interface TodotxtTaskParser {

  Task parse(String line);
}
