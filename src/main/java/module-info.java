module org.egedede.todotxt {
  opens org.egedede.todotxt;
  opens org.egedede.todotxt.javafx;

  requires javafx.controls;
  requires org.tinylog.api;
}